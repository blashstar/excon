$(document).foundation();
/*
$(document).ready(function($){

	$('#crono').dsCountDown({
		endDate: new Date("October 11, 2016 10:00:00"),
		titleDays: 'Días',
		titleHours: 'Horas',
		titleMinutes: 'Minutos',
		titleSeconds: 'Segundos'
	});

});
*/


$('#crono').timeTo({
    timeTo: new Date("October 11, 2016 10:00:00"),
    displayDays: 3,
    displayCaptions: true,
    lang: "sp",
    captionSize: 8,
    fontSize: 22
});

var $social = $('#social');
$social.on('click', '.tw', function (evento) {
	console.debug($social);
	$social.toggleClass('visible');
	evento.preventDefault();
});
